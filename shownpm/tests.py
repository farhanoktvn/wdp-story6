from django.test import TestCase, Client
from django.urls import resolve

class ShowNPM(TestCase):

    def test_showNPM_response(self):
        response = Client().get('/shownpm/')
        self.assertEqual(response.status_code, 200)

    def test_showNPM_template(self):
        response = Client().get('/shownpm/')
        self.assertTemplateUsed(response, 'shownpm.html')

    def test_shownpm_npm_exist(self):
        response = Client().get('/shownpm/')
        html_response = response.content.decode('utf8')
        self.assertIn('1806173512', html_response)
