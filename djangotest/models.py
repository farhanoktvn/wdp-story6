from django.db import models

class Status(models.Model):
    status = models.CharField(max_length=280, null=False)
    created_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.status
