from django.shortcuts import (
    render,
    redirect,
)
from .forms import StatusForm
from .models import Status

def index(request):
    if request.method == "POST":
        form = StatusForm(request.POST)
        if (form.is_valid()):
            test_status = Status()
            test_status.status = form.cleaned_data['status']
            test_status.save()
        return redirect('index')
    else:
        form = Status()
    status_list = Status.objects.all()
    response = {
        'status': status_list,
    }
    return render(request, 'index.html', response)
