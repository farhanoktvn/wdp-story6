from django import forms
from .models import Status

class StatusForm(forms.Form):

    status = forms.CharField(widget=forms.TextInput(attrs={
        'type' : 'text',
        'class' : 'form-control',
        'maxlength' : '280',
        'required' : True,
        'placeholder' : 'What\'s on your mind?',
        'label': ''
    }))
