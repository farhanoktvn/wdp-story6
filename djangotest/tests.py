from django.test import TestCase, Client
from django.apps import apps
from django.urls import resolve

from .views import index
from .models import Status
from .forms import StatusForm

class DjangoTest(TestCase):

    def test_index_response(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_index_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'index.html')

    def test_index_using_function_index(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_index_text_is_correct(self):
        response = Client().get('')
        html_response = response.content.decode('utf8')
        self.assertIn('Hello, how are you?', html_response)

    def test_create_model_status(self):
        test_status = Status.objects.create(status="This is a status.")
        self.assertEqual(Status.objects.all().count(), 1)

    def test_model_status_return_string_status(self):
        test_status = Status.objects.create(status="This is a status.")
        self.assertEqual(str(test_status), test_status.status)

    def test_validate_status_created_by_POST(self) :
        response = Client().post('', {'status': 'This is a status'}, follow=True)
        html_response = response.content.decode('utf8')
        self.assertIn('This is a status', html_response)

    def test_model_status_can_not_be_created_without_status(self):
        test_status = StatusForm({'status': ''})
        self.assertFalse(test_status.is_valid())
